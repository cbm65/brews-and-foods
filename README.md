# Brews and Foods - How to Run

Must use Docker
Clone project into local directory
Run the following terminal commands:

- docker volume create beta-data
- docker-compose build
- docker-compose up
- docker exec -it brews-and-foods-brewery-api-1 bash
- python manage.py loaddata brewery_project/data.json

wait like 15 minutes...

Go to http://localhost:3000/ in your browser
Enjoy!
