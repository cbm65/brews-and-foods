from django.contrib import admin
from .models import Brewery, State, City






admin.site.register(Brewery)
admin.site.register(State)
admin.site.register(City)
