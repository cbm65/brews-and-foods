from common.json import ModelEncoder

from .models import Brewery


class BreweryEncoder(ModelEncoder):
    model = Brewery
    properties = [
        "id",
        "name",
        "type",
        "street",
        "city",
        "state",
        "county_province",
        "postal_code",
        "country",
        "longitude",
        "latitude",
        "phone",
        "website_url",
        "picture_url",
    ]
