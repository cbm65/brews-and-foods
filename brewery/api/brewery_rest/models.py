from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class Brewery(models.Model):

        obdb_id= models.CharField(max_length=200, null=True)
        name= models.CharField(max_length=200, null=True)
        type= models.CharField(max_length=200, null=True)
        street= models.CharField(max_length=200, null=True)
        address_2= models.CharField(max_length=200, null=True)
        address_3= models.CharField(max_length=200, null=True)
        city= models.CharField(max_length=200, null=True)
        state= models.CharField(max_length=200, null=True)
        county_province= models.CharField(max_length=200, null=True)
        postal_code= models.CharField(max_length=200, null=True)
        country= models.CharField(max_length=200, null=True)
        longitude= models.CharField(max_length=200, null=True)
        latitude= models.CharField(max_length=200, null=True)
        phone= models.CharField(max_length=200, null=True)
        website_url= models.URLField(null=True)
        updated_at= models.CharField(max_length=200, null=True)
        created_at= models.CharField(max_length=200, null=True)
        picture_url= models.URLField(null=True)

        def get_api_url(self):
            return reverse("api_brewery", kwargs={"pk": self.id})

        def __str__(self):
            return self.name

class State(models.Model):

    id = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=40)
    abbreviation = models.CharField(max_length=2, unique=True)

    def __str__(self):
        return f"{self.abbreviation}"

    class Meta:
        ordering = ("abbreviation",)

class City(models.Model):


    name = models.CharField(max_length=40)
    state = models.CharField(max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)
