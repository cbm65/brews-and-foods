from django.urls import path

from .views import (

    api_brewerys,
    api_brewery,
    api_list_states,
    api_list_cities,

)

urlpatterns = [

    path(
        "brewerys/",
        api_brewerys,
        name="api_brewerys",
    ),
    path(
        "brewerys/<int:pk>/",
        api_brewery,
        name="api_brewery",
    ),
    path("states/", api_list_states, name="api_list_states"),
    path("cities/", api_list_cities, name="api_list_cities"),
]
