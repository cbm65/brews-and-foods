from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (

    BreweryEncoder,

)
from .models import Brewery, State, City


@require_http_methods(["GET", "POST"])
def api_brewerys(request):
    if request.method == "GET":
        brewerys = Brewery.objects.all()
        return JsonResponse(
            {"brewerys": brewerys},
            encoder=BreweryEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            brewery = Brewery.objects.create(**content)
            return JsonResponse(
                brewery,
                encoder=BreweryEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the brewery"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_brewery(request, pk):
    if request.method == "GET":
        try:
            brewery = Brewery.objects.get(id=pk)
            return JsonResponse(
                brewery,
                encoder=BreweryEncoder,
                safe=False
            )
        except Brewery.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            brewery = Brewery.objects.get(id=pk)
            brewery.delete()
            return JsonResponse(
                brewery,
                encoder=BreweryEncoder,
                safe=False,
            )
        except Brewery.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            brewery = Brewery.objects.get(id=pk)

            props = [

                "id",
                "name",
                "type",
                "street",
                "city",
                "state",
                "county_province",
                "postal_code",
                "country",
                "longitude",
                "latitude",
                "phone",
                "website_url",
                "picture_url",

            ]
            for prop in props:
                if prop in content:
                    setattr(brewery, prop, content[prop])
            brewery.save()
            return JsonResponse(
                brewery,
                encoder=BreweryEncoder,
                safe=False,
            )
        except Brewery.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET"])
def api_list_states(request):
    states = State.objects.order_by("name")
    state_list = []
    for state in states:
        values = {
            "name": state.name,
            "abbreviation": state.abbreviation,
        }
        state_list.append(values)
    return JsonResponse({"states": state_list})

@require_http_methods(["GET"])
def api_list_cities(request):
    cities = City.objects.order_by("name")
    city_list = []
    for city in cities:
        values = {
            "name": city.name,
            "state": city.state,
        }
        city_list.append(values)
    return JsonResponse({"cities": city_list})
