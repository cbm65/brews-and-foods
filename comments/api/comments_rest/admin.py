from django.contrib import admin

from .models import Comment, BreweryVO, FoodVO


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass

@admin.register(BreweryVO)
class BreweryVOAdmin(admin.ModelAdmin):
    pass

@admin.register(FoodVO)
class FoodVOAdmin(admin.ModelAdmin):
    pass
