from common.json import ModelEncoder

from .models import Comment, BreweryVO, FoodVO

class BreweryVOEncoder(ModelEncoder):
    model = BreweryVO
    properties = [
        "name",
        "import_href",
    ]

class FoodVOEncoder(ModelEncoder):
    model = FoodVO
    properties = [
        "name",
        "import_href",
    ]

class CommentEncoder(ModelEncoder):
    model = Comment
    properties = [
        "id",
        "brewery",
        "food",
        "brewery_item",
        "food_item",
        "body",
        "picture_url",

    ]
    encoders = {
        "brewery": BreweryVOEncoder(),
        "food": FoodVOEncoder(),
    }
