from django.urls import path

from .views import (

    api_comments,
    api_comment
)

urlpatterns = [

    path(
        "comments/",
        api_comments,
        name="api_comments",
    ),
    path(
        "comments/<int:pk>/",
        api_comment,
        name="api_comment",
    ),
]
