from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (

    CommentEncoder,

)
from .models import Comment, FoodVO, BreweryVO


@require_http_methods(["GET", "POST"])
def api_comments(request):
    if request.method == "GET":
        comments = Comment.objects.all()
        return JsonResponse(
            {"comments": comments},
            encoder=CommentEncoder,
        )
    else:
        try:

            content = json.loads(request.body)
            food_href = content["food"]
            food = FoodVO.objects.get(import_href=food_href)
            content["food"] = food
            brewery_href = content["brewery"]
            brewery = BreweryVO.objects.get(import_href=brewery_href)
            content["brewery"] = brewery
            comment = Comment.objects.create(**content)

            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the comment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_comment(request, pk):
    if request.method == "GET":
        try:
            comment = Comment.objects.get(pk=pk)
            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False
            )
        except Comment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            comment = Comment.objects.get(pk=pk)
            comment.delete()
            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False,
            )
        except Comment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            comment = Comment.objects.get(pk=pk)

            props = [

                "id",
                "brewery_item",
                "food_item",
                "body",
                "picture_url",

            ]
            for prop in props:
                if prop in content:
                    setattr(comment, prop, content[prop])
            comment.save()
            return JsonResponse(
                comment,
                encoder=CommentEncoder,
                safe=False,
            )
        except Comment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response





