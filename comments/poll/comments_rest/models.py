from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class BreweryVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class FoodVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class Comment(models.Model):

    brewery_item = models.CharField(max_length=200, null=True)
    food_item = models.CharField(max_length=200, null=True)
    picture_url = models.URLField()
    body = models.TextField()

    brewery = models.ForeignKey(
        BreweryVO,
        related_name="brewerys",
        on_delete=models.PROTECT,
    )

    food = models.ForeignKey(
        FoodVO,
        related_name="foods",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_comment", kwargs={"pk": self.id})
