import django
import os
import sys
import time
import json
import requests



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "comments_project.settings")
django.setup()

# Import models from comments_rest, here.
# from comments_rest.models import Something
from comments_rest.models import BreweryVO, FoodVO

def get_foods():
    response = requests.get("http://foods-api:8000/api/foods/")
    content = json.loads(response.content)
    for food in content["foods"]:
        FoodVO.objects.update_or_create(
            import_href=food["href"],
            defaults={"name": food["name"]},
        )

def get_brewerys():
    response = requests.get("http://brewery-api:8000/api/brewerys/")
    content = json.loads(response.content)
    for brewery in content["brewerys"]:
        BreweryVO.objects.update_or_create(
            import_href=brewery["href"],
            defaults={"name": brewery["name"]},
        )


def poll():
    while True:
        try:
            get_foods()
            get_brewerys()
        except Exception as e:
            print(e)
        time.sleep(5)


if __name__ == "__main__":
    poll()
