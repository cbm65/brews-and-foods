from common.json import ModelEncoder

from .models import Food


class FoodEncoder(ModelEncoder):
    model = Food
    properties = [
        "id",
        "name",
        "city",
        "state",
        "phone",
        "website_url",
        "picture_url",
    ]
