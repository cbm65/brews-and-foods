from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class Food(models.Model):

        name = models.CharField(max_length=200, null=True)
        city = models.CharField(max_length=200, null=True)
        state = models.CharField(max_length=200, null=True)
        phone = models.CharField(max_length=200, null=True)
        website_url = models.URLField(null=True)
        picture_url = models.URLField(null=True)

        def get_api_url(self):
            return reverse("api_food", kwargs={"pk": self.id})

        def __str__(self):
            return self.name
