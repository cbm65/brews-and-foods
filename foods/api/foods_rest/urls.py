from django.urls import path

from .views import (
    api_food,
    api_foods,

)

urlpatterns = [
    
    path(
        "foods/",
        api_foods,
        name="api_foods",
    ),
    path(
        "foods/<int:pk>/",
        api_food,
        name="api_food",
    ),

]
