from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (

    FoodEncoder,

)
from .models import Food


@require_http_methods(["GET", "POST"])
def api_foods(request):
    if request.method == "GET":
        foods = Food.objects.all()
        return JsonResponse(
            {"foods": foods},
            encoder=FoodEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            food = Food.objects.create(**content)
            return JsonResponse(
                food,
                encoder=FoodEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the food"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_food(request, pk):
    if request.method == "GET":
        try:
            food = Food.objects.get(id=pk)
            return JsonResponse(
                food,
                encoder=FoodEncoder,
                safe=False
            )
        except Food.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            food = Food.objects.get(id=pk)
            food.delete()
            return JsonResponse(
                food,
                encoder=FoodEncoder,
                safe=False,
            )
        except Food.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            food = Food.objects.get(id=pk)

            props = [

                "id",
                "name",
                "food_type",
                "street",
                "city",
                "state",
                "county_province",
                "postal_code",
                "country",
                "longitude",
                "latitude",
                "phone",
                "website_url",
                "picture_url",

            ]
            for prop in props:
                if prop in content:
                    setattr(food, prop, content[prop])
            food.save()
            return JsonResponse(
                food,
                encoder=FoodEncoder,
                safe=False,
            )
        except Food.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
