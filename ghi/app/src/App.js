import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useState, useEffect } from "react";
import BreweryList from './ListBrewerys';

function App() {

  const [brewerys, setBrewerys] = useState([])

  const getBrewerys = async () => {
    const url = 'http://localhost:8100/api/brewerys/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const brewerys = data.brewerys
      setBrewerys(brewerys)
    }
  }

  const [foods, setFoods] = useState([])

  const getFoods = async () => {
    const url = 'http://localhost:8080/api/foods/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const foods = data.foods
      setFoods(foods)
    }
  }

  const [comments, setComments] = useState([])

  const getComments = async () => {
    const url = 'http://localhost:8090/api/comments/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const comments = data.comments
      setComments(comments)
    }
  }

  const [states, setStates] = useState([])

  const getStates = async () => {
    const url = 'http://localhost:8100/api/states/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const states = data.states
      setStates(states)
    }
  }

  const [cities, setCities] = useState([])

  const getCities = async () => {
    const url = 'http://localhost:8100/api/cities/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const cities = data.cities
      setCities(cities)
    }
  }

  useEffect(() => {

    getBrewerys();
    getFoods();
    getComments();
    getStates();
    getCities();

  }, []);



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="brewerys">
            <Route index element={<BreweryList brewerys={brewerys} getBrewerys={getBrewerys} />} />
            {/* <Route path="new" element={<BreweryForm getBrewerys={getBrewerys}/>} /> */}
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
  }
export default App;
