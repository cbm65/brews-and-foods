import React, {useEffect, useState} from "react"
import { Link } from "react-router-dom";


function BreweryList({ brewerys, getBrewerys}) {


    const [state, setState] = useState("");
    const [states, setStates] = useState([]);
    const [city, setCity] = useState("");
    const [cities, setCities] = useState([]);

    let newCities = []

    for (let i = 0; i < cities.length; i++)

        if (cities[i]["state"] === state)

            newCities.push(cities[i])

    let newBrewerys = []

    if (city === "")

      for (let i = 0; i < brewerys.length; i++) {

          if (brewerys[i]["state"] === state)

              newBrewerys.push(brewerys[i])
      }

    else

      for (let i = 0; i < brewerys.length; i++) {

          if (brewerys[i]["state"] === state && brewerys[i]["city"] === city)

              newBrewerys.push(brewerys[i])
      }

    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
        setCity("");
      }

      const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
      }


    const fetchData = async () => {
    const url = 'http://localhost:8100/api/states/';
    const url1 = 'http://localhost:8100/api/cities/';


    const response = await fetch(url);
    const response1 = await fetch(url1);


    if (response.ok) {
        const data = await response.json();
        setStates(data.states)
    }

    if (response1.ok) {
      const data1 = await response1.json();
      setCities(data1.cities)
  }

  }

    useEffect(() => {
        fetchData();
    }, []);


//     const deleteBrewery = async (id) => {
//         fetch(`http://localhost:8090/api/brewerys/${id}/`, {
//         method:"delete",
//         })
//         .then(() => {
//         return getBrewerys()
//         })
//     }

    if (brewerys === undefined) {
        return null
    }


  return (
  <>
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Filter by Location</h1>
            <form id="create-brewery-form">

                <div className="mb-3">
                    <select onChange = {handleStateChange} value={state} required id="state" name="state" className="form-select">
                    <option value="">choose a state</option>
                    {states.map(state => {
                        return (
                        <option key={state.id} value={state.name}>
                        {state.name}
                        </option>
                        );
                        })}
                    </select>
                </div>

                <div className="mb-3">
                    <select onChange = {handleCityChange} value={city} required id="city" name="city" className="form-select">
                    <option value="">choose a city</option>
                    {newCities.map(city => {
                        return (
                        <option key={city.id} value={city.name}>
                        {city.name}
                        </option>
                        );
                        })}
                    </select>
                </div>

            </form>
          </div>
        </div>
      </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Type</th>
          <th>State</th>
          <th>City</th>
        </tr>
      </thead>
      <tbody>
        {newBrewerys.map((brewery) => {
          return (
            <tr key={brewery.id} >
              <td>{ brewery.name } </td>
              <td>{ brewery.type }</td>
              <td>{ brewery.state }</td>
              <td>{ brewery.city }</td>
              {/* <td>
                  <button type="button" value={brewery.id} onClick={() => deleteBrewery(brewery.id)}>Delete</button>
              </td> */}
            </tr>
          );
        })}
      </tbody>
    </table>
  </>
  );
}

export default BreweryList;
